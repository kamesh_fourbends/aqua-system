<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Temperature extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function __construct(){
		parent::__construct();
		$this->table = "BSKP-DHTSR";
	}
	
	public function index()
	{
		if($this->fb_rest->isloggedin()){
		$data = array();
		$page_no = $this->uri->segment('2');
		$per_page = $this->input->get_post("no_items", true);
		$search = $this->input->get_post("search", true);
		$sort_fld = $this->input->get_post("sort_fld", true);
		$sort_dir = $this->input->get_post("sort_dir", true);
		$page_burl = site_url("/temperature");
		$table_name = $this->table;
		$params =  array("page_no" => $page_no, "per_page" => $per_page, "uri_segment" => "2",
		"search" => $search, "sort_fld" => $sort_fld, "sort_dir" => $sort_dir, "page_burl" => $page_burl, "table_name" => $table_name);
		
		  
  		$data["sort_fld"] = $sort_fld;
		$data["sort_dir"] = $sort_dir;
		$data["search"] = $search;
		$data["per_page"] = $per_page;
		
		$sort_columns = array("temp_value", "createdtime");
		
		$hstr = array("temp_value" => fb_text("temp_value"), "createdtime" => fb_text("createdtime"),
		"action" => fb_text("action")
		);
		
		$theader = "";
		
		foreach($hstr as $hk => $hv)
		{
			if(in_array($hk, $sort_columns)){
				$cdir = ($hk == $sort_fld) ? (($sort_dir=="asc") ? "desc" : "asc" ) : "asc";
				$pstr = (!empty($per_page)) ? $per_page : "10";
				$srt_params = array("sort_fld" => $hk, "sort_dir" => $cdir, "no_items" => $pstr, "search" => $search);
				$srt_str = http_build_query($srt_params);
				$srt_url = site_url("/temperature?$srt_str");
				$cdir_icon = "";
				if(!empty($sort_fld)){
					$cdir_icon = ($hk == $sort_fld) ? 
					(($sort_dir=="asc") ? "&nbsp;<i class=\"fa fa-sort-asc\"></i>" : "&nbsp;<i class=\"fa fa-sort-desc\"></i>" ) : "";
				}
				$thstr = $hv.$cdir_icon;
				$thtml = "<th><a href='$srt_url'>$thstr</a></th>";
				$theader .= $thtml."\n";
			}else{
				$theader .= "<th>$hv</th>\n";
			}
		}
		
		$data["theader"] = $theader;
		
		$msg  = $this->fb_rest->list_record($params);
		

		
  		    $this->load->view('include/header');
			$this->load->view('include/left_menu');
			
			//fb_pr($msg);
			if($msg["status"] == "success")
			{
				$data["page_links"] = $msg["page_links"];
				$data["result_set"] = $msg["result_set"];
				$this->load->view("layout/temperature_content", $data);
			}else{
				$this->load->view("layout/error", $data);
			}
		
			$this->load->view('include/footer');
		}else{
			redirect('/login');
		}
	}
	
}
