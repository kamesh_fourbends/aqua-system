<input type="hidden" id="base" value="<?php echo base_url(); ?>">
<script src="<?php echo base_url();?>assets/js/vendor/jquery-2.1.4.min.js"></script>
<script src="<?php echo base_url();?>assets/js/popper.min.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/chart-js/Chart.bundle.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script> 
<script src="<?php echo base_url();?>assets/js/dashboard.js"></script>
<script src="<?php echo base_url();?>assets/js/widgets.js"></script>
<script src="<?php echo base_url();?>assets/js/datepicker/moment.js"></script> 
<script src="<?php echo base_url();?>assets/js/bootstrap-datetimepicker.js"></script> 
<script src="<?php echo base_url();?>assets/js/jquery.floatThead.min.js"></script>
<script src="<?php echo base_url();?>assets/js/main.js"></script>
</body>
</html>