<!-- Left Panel -->

<aside id="left-panel" class="left-panel">
  <nav class="navbar navbar-expand-sm navbar-default">
    <div class="navbar-header">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation"> <i class="fa fa-bars"></i> </button>
      <a class="navbar-brand" href="./"><img src="<?php echo base_url();?>assets/images/logo.png" alt="Logo"></a> <a class="navbar-brand hidden" href="./"><img src="<?php echo base_url();?>assets/images/logo2.png" alt="Logo"></a> </div>
    <div id="main-menu" class="main-menu collapse navbar-collapse">
      <ul class="nav navbar-nav">
        <li class="active"> <a href="<?php echo site_url("/dashboard");?>" id="dashboard"> <i class="menu-icon fa fa-dashboard"></i><?php echo fb_text("dashboard"); ?> </a> </li>
        <h3 class="menu-title"><?php echo fb_text("menus"); ?> </h3>
        <!-- /.menu-title -->
        
        <li class="menu-item-has-children dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="ponds"><i class="menu-icon fa fa-th"></i><?php echo fb_text("ponds"); ?></a>
          <ul class="sub-menu children dropdown-menu">
            <li><i class="fa fa-list"></i><a href="<?php echo site_url("/pondlist");?>" id="pondlist"><?php echo fb_text("pond_list"); ?></a></li>
            <li><i class="fa fa-list"></i><a href="<?php echo site_url("/pondcleaning");?>" id="pond_cleaning"><?php echo fb_text("pond_cleaning"); ?></a></li>
          </ul>
        </li>
        <li> <a href="<?php echo site_url("/species");?>" id="species"><i class="menu-icon fa fa-odnoklassniki"></i><?php echo fb_text("species"); ?> </a> </li>
        <li class="menu-item-has-children dropdown" > <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="feed"><i class="menu-icon fa fa-pagelines"></i><?php echo fb_text("feed"); ?></a> 
         <ul class="sub-menu children dropdown-menu">
          <li><i class="fa fa-list"></i><a href="<?php echo site_url("/feed");?>" id="feed"><?php echo fb_text("feed_list"); ?></a></li>
            <li><i class="fa fa-list"></i><a href="<?php echo site_url("feed/feedStock");?>" id="feedstock"><?php echo fb_text("feed_stock_list"); ?></a></li>
            <li><i class="fa fa-list"></i><a href="<?php echo site_url("feed/feedDistribution");?>" id="feeddist"><?php echo fb_text("feed_distribution"); ?></a></li>
          </ul>
        </li>
        <li class="menu-item-has-children dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="stock"><i class="menu-icon fa fa-signal"></i><?php echo fb_text("stock"); ?></a>
          <ul class="sub-menu children dropdown-menu">
            <li><i class="fa fa-list"></i><a href="<?php echo site_url("/currentstock");?>" id="current_stock"><?php echo fb_text("current_stock"); ?></a></li>
            <li><i class="fa fa-list"></i><a href="<?php echo site_url("/stock");?>" id="fish_stock"><?php echo fb_text("fish_stock"); ?></a></li>
          </ul>
        </li>
        <li> <a href="<?php echo site_url("/distribution");?>" id="distribution"><i class="menu-icon fa fa-clone"></i><?php echo fb_text("distribution"); ?></a> </li>
        <li> <a href="<?php echo site_url("/harvest");?>" id="harvest"><i class="menu-icon fa fa-archive"></i><?php echo fb_text("harvest"); ?></a> </li>
        <li> <a href="<?php echo site_url("/mortality");?>" id="mortality"><i class="menu-icon fa fa-laptop"></i><?php echo fb_text("mortality"); ?> </a> </li>
		<li> <a href="<?php echo site_url("/temperature");?>" id="temperature"><i class="menu-icon fa fa-thermometer-half"></i><?php echo fb_text("temperature"); ?> </a> </li>
		
	    <li class="menu-item-has-children dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="server"><i class="menu-icon fa fa-server"></i><?php echo fb_text("server"); ?></a>
          <ul class="sub-menu children dropdown-menu">
		   <li><i class="fa fa-cog"></i><a href="<?php echo site_url("/common/settings");?>" id="settings"><?php echo fb_text("settings"); ?></a></li>
		   <li><i class="fa fa-cog"></i><a href="<?php echo site_url("/common/purge_caches");?>" id="purge_caches"><?php echo fb_text("purge_all_caches"); ?></a></li>
          </ul>
        </li>
		
      </ul>
    </div>
    <!-- /.navbar-collapse --> 
  </nav>
</aside>
<!-- /#left-panel --> 

<!-- Left Panel --> 
