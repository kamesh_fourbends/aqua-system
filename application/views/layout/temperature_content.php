<div id="right-panel" class="right-panel">

<?php $this->load->view('./include/top_menu'); ?>

<div class="breadcrumbs">
<div class="col-sm-4">
  <div class="page-header float-left">
    <div class="page-title">
      <h1><?php echo fb_text("temperature"); ?></h1>
    </div>
  </div>
</div>
<div class="col-sm-8">
  <div class="page-header float-right">
    <div class="page-title">
      <ol class="breadcrumb text-right">
        <li><a href="<?php echo site_url("/dashboard");?>"><?php echo fb_text("dashboard"); ?></a></li>
        <li class="active"><?php echo fb_text("temperature"); ?></li>
      </ol>
    </div>
  </div>
</div>
</div>

<div class="content mt-3">
<div class="animated fadeIn">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header"> <strong class="card-title"><?php echo fb_text("temperature"); ?></strong> </div>
        <div class="card-body">
             <?php if($this->session->flashdata('delete_success')) {
			  ?>
			  <div class="sufee-alert alert with-close alert-success alert-dismissible fade show"> <?php echo $this->session->flashdata('delete_success');  ?>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			  </div>
			  <?php } ?>
			  <?php if($this->session->flashdata('delete_failed')) {
			  ?>
			  <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show" data-dismiss="alert"> <?php echo $this->session->flashdata('delete_failed');  ?>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			  </div>
			  <?php } ?>         
          <div id="bootstrap-data-table_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
          			<form name="search" id="srchfrm" action="<?php echo site_url("/temperature/"); ?>" method="GET">			 
				   <div class="row">
					  <div class="col-sm-12 col-md-6">
						 <div class="dataTables_length" id="bootstrap-data-table_length">
							<label>
							   <?php echo fb_text("show"); ?> 
							   <select name="no_items" aria-controls="bootstrap-data-table" class="form-control form-control-sm">
								 <?php
							   $arr=array(10=>'10',20=>"20",50=>"50",-1=>"All");
							   foreach($arr as $key=>$val)
							   {							   
								   $selected='';
	   							   if(isset($_GET["no_items"]) and $_GET["no_items"]==$key)
								   $selected='selected="selected"';
								   echo '<option '.$selected.' value="'.$key.'">'.$val.'</option>';
							   }							   
							    ?>
							   </select>
							   <?php echo fb_text("entries"); ?> 
							</label>
						 </div>
					  </div>
					  <div class="col-sm-12 col-md-6">
					   <div id="bootstrap-data-table_filter" class="dataTables_filter"><label><?php echo fb_text("search").":"; ?>
						   <input type="search" name="search" class="form-control form-control-sm" placeholder="" value="<?php echo $search; ?>" />
						</label></div>
                         <input type="hidden" name="sort_fld" value="<?php echo $sort_fld; ?>" />
						 <input type="hidden" name="sort_dir" value="<?php echo $sort_dir; ?>" />
					  </div>
				   </div>
                   </form>
							   
			   <div class="row">
				  <div class="col-sm-12">
					 <table class="table table-striped table-bordered no-footer" role="grid" id="temperature-table">
						<thead>
						   <tr role="row">
							  <?php echo $theader; ?>
						   </tr>
						</thead>
						<tbody>

						   
						   <?php foreach($result_set as $key=>$row): 
							  $source = $row["_source"];
							  $rkey = $row["_id"];
							  
						   ?>
						     <tr role="row">
							  <td class=""><?php echo $source["temp_value"]; ?></td>
							  <td><?php echo fb_convert_jsdate($source["createdtime"]); ?></td>
							  <td><a href="#" data-id="<?php echo $rkey; ?>" class="view-modal" data-toggle="modal" data-target="#view-modal"><i class="fa fa-eye"></i></a></td>
                              
						    </tr>
						   <?php endforeach; ?>
						</tbody>
					 </table>
				  </div>
			   </div>
			   <div class="row">
				  <div class="col-sm-12 col-md-5">
					 <!--<div class="dataTables_info" id="bootstrap-data-table_info" role="status" aria-live="polite">Showing 11 to 20 of 25 entries</div>-->
				  </div>
				  <div class="col-sm-12 col-md-7">
					 <div class="dataTables_paginate paging_simple_numbers" id="bootstrap-data-table_paginate">
					    <?php echo $page_links; ?>

					 </div>
				  </div>
			   </div>
			</div>
        </div>
      </div>
      
    </div>
    
 </div>
</div>
<!-- .animated --> 
</div>

</div><!-- /#right-panel -->

<!-- Right Panel -->
<!-- View Modal --> 
<div class="modal fade" id="view-modal" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="temperaturemodel"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <p><label><?php echo fb_text("createdtime"); ?>:</label>&nbsp;&nbsp; <span id="createdtimemodel"></span></p>
           
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo fb_text("close"); ?></button>
        </div>
    </div>
</div>
</div>