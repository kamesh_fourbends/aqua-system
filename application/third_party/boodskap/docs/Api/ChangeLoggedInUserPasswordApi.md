# Swagger\Client\ChangeLoggedInUserPasswordApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**changeMyPassword**](ChangeLoggedInUserPasswordApi.md#changeMyPassword) | **POST** /user/changemypass/{atoken}/{pswd} | Change password of logged in user


# **changeMyPassword**
> \Swagger\Client\Model\Success changeMyPassword($atoken, $pswd)

Change password of logged in user

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ChangeLoggedInUserPasswordApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in user
$pswd = "pswd_example"; // string | New password of the user

try {
    $result = $apiInstance->changeMyPassword($atoken, $pswd);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ChangeLoggedInUserPasswordApi->changeMyPassword: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **pswd** | **string**| New password of the user |

### Return type

[**\Swagger\Client\Model\Success**](../Model/Success.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

