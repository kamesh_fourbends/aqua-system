# Swagger\Client\CreateUpdateDomainDeviceGroupApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**upsertDomainDeviceGroup**](CreateUpdateDomainDeviceGroupApi.md#upsertDomainDeviceGroup) | **POST** /domain/device/group/upsert/{atoken} | Create / Update Domain Device Group


# **upsertDomainDeviceGroup**
> \Swagger\Client\Model\Success upsertDomainDeviceGroup($atoken, $entity)

Create / Update Domain Device Group

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CreateUpdateDomainDeviceGroupApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in user
$entity = new \Swagger\Client\Model\DomainDeviceGroup(); // \Swagger\Client\Model\DomainDeviceGroup | DomainDeviceGroup JSON object

try {
    $result = $apiInstance->upsertDomainDeviceGroup($atoken, $entity);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CreateUpdateDomainDeviceGroupApi->upsertDomainDeviceGroup: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **entity** | [**\Swagger\Client\Model\DomainDeviceGroup**](../Model/DomainDeviceGroup.md)| DomainDeviceGroup JSON object |

### Return type

[**\Swagger\Client\Model\Success**](../Model/Success.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

