# MessageHeader

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **string** | domain key | 
**api** | **string** | domain api-key | 
**did** | **string** | device id | 
**dmdl** | **string** | device model | 
**fwver** | **string** | device firmware version | 
**mid** | **int** | message identifier | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


