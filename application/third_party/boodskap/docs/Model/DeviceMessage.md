# DeviceMessage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**header** | **string** | message header JSON object | 
**data** | **string** | message payload JSON object | 
**received_stamp** | **int** |  | [optional] 
**id** | **string** |  | [optional] 
**message_id** | **int** |  | [optional] 
**node_id** | **string** |  | [optional] 
**node_uuid** | **string** |  | [optional] 
**device_model** | **string** |  | [optional] 
**firmware_version** | **string** |  | [optional] 
**ip_address** | **string** |  | [optional] 
**port** | **int** |  | [optional] 
**channel** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


