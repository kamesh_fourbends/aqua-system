# Vertical

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**domain_key** | **string** |  | [optional] 
**client_domain_key** | **string** |  | [optional] 
**category** | **string** |  | [optional] 
**tags** | **string** |  | [optional] 
**verticalid** | **string** |  | [optional] 
**verticalname** | **string** |  | [optional] 
**verticalimage** | **string** |  | [optional] 
**pricing** | **bool** |  | [optional] 
**price** | **double** |  | [optional] 
**version** | **string** |  | [optional] 
**enabled** | **bool** |  | [optional] 
**published** | **bool** |  | [optional] 
**description** | **string** |  | [optional] 
**createdby** | **string** |  | [optional] 
**createdbyemail** | **string** |  | [optional] 
**createdtime** | **int** |  | [optional] 
**updatedtime** | **int** |  | [optional] 
**importedtime** | **int** |  | [optional] 
**messages** | **int[]** |  | [optional] 
**records** | **int[]** |  | [optional] 
**messagerules** | **int[]** |  | [optional] 
**scheduledrules** | **int[]** |  | [optional] 
**namedrules** | **string[]** |  | [optional] 
**templates** | **string[]** |  | [optional] 
**dashboards** | [**\Swagger\Client\Model\Dashboard[]**](Dashboard.md) |  | [optional] 
**mobiledashboards** | [**\Swagger\Client\Model\Dashboard[]**](Dashboard.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


