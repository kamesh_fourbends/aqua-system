# MLModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**relation** | **string** |  | 
**classifier** | **string** |  | 
**prediction_type** | **string** |  | 
**train_every** | **int** |  | 
**created_at** | **int** |  | [optional] 
**trained_at** | **int** |  | [optional] 
**attributes** | [**\Swagger\Client\Model\MLAttribute[]**](MLAttribute.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


