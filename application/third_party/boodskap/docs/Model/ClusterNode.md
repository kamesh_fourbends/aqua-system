# ClusterNode

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**node_id** | **string** |  | [optional] 
**node_uid** | **string** |  | [optional] 
**consistent_id** | **string** |  | [optional] 
**addresses** | **string[]** |  | [optional] 
**host_names** | **string[]** |  | [optional] 
**props** | [**\Swagger\Client\Model\NodeProperty[]**](NodeProperty.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


